# Using Verity Agent with REST API

Sample code for the third Condatis webinar on decentralized identity
[Using Hyperledger Aries to Issue and Verify Credentials](https://youtu.be/SLcxd_dEG-c).

## Pre-requisites

- Docker host
- Ngrok
- Verity API key and domain DID
  (see [Evernym's Verity documentation](https://github.com/evernym/verity-sdk/tree/main/verity#verity-in-docker) to learn how to run Verity service locally)
